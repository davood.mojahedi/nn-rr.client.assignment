import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuoteViewerComponent } from './components/quote-viewer/quote-viewer.component';

const routes: Routes = [
  {
    path: 'quote',
    component: QuoteViewerComponent
  },
  {
    path: '**',
    redirectTo: 'quote',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
