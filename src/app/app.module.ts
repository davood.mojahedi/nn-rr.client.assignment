import { ConfigService } from './../service/config.service';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { QuoteViewerComponent } from './components/quote-viewer/quote-viewer.component';
import { HttpClientModule } from '@angular/common/http';
import { DropdownDirective } from './directives/dropdown.directive';
import { FormsModule } from '@angular/forms';

export function initializerFunction(configServcie: ConfigService) {
  return () => {
    configServcie.load();
  };
}

@NgModule({
  declarations: [
    QuoteViewerComponent,
    DropdownDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [ConfigService],
      useFactory: initializerFunction,
    }
  ],
  bootstrap: [QuoteViewerComponent]
})
export class AppModule { }
