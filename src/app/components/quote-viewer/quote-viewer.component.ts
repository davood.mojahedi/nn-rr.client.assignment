import { Quote } from './../../../data/quote';
import { QuoteInfoService } from './../../../service/quote-info.service';
import { Company } from './../../../data/company';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quote-viewer',
  templateUrl: './quote-viewer.component.html',
  styleUrls: ['./quote-viewer.component.css']
})
export class QuoteViewerComponent implements OnInit {

  constructor(private quoteInfoService:QuoteInfoService) { }

  public allCompanies : Company[] = new Array<Company>();
  public displayedCompanies : Company[] = new Array<Company>();
  public selectedQuote : Quote = new Quote();
  public inputCompany:string;

  public selectCompany(selectedCompany : Company){
    this.quoteInfoService.getQuoteInfo(selectedCompany.symbol)
    .subscribe(result=>{
      this.selectedQuote = result;
      this.inputCompany = this.selectedQuote.symbol;
      this.companyChange();
    })
  }

  public companyChange(){
    if(!this.inputCompany) {
      this.displayedCompanies = this.allCompanies;
      return;
    }

    this.displayedCompanies = this.allCompanies
    .filter(item=>{
      return item.name.toLowerCase().includes(this.inputCompany.toLowerCase()) || item.symbol.toLowerCase().includes(this.inputCompany.toLowerCase());
    })
  }

  ngOnInit(): void {
    
    setTimeout(()=>{
      this.quoteInfoService.getAllCompanies()
      .subscribe(result=>{
        this.allCompanies  = result;
        this.displayedCompanies = result;
      })
    },2000)
    
  }

}
