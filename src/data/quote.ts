export class Quote {
    public name:string;
    public symbol:string;
    public lastPrice:number;
    public change:number;
    public changePercent:number;
    public timestamp:Date;
    public msDate:number;
    public marketCap:number;
    public volume:number;
    public changeYTD:number;
    public changePercentYTD :number;
    public high:number;
    public low:number;
    public open:number;
}