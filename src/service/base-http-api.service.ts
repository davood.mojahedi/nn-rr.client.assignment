import { ConfigService } from "./config.service";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Observer } from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    "timeout": "30000"
  }),
};

@Injectable({
  providedIn: "root",
})
export class BaseHttpApiService {
  constructor(private http: HttpClient,
    private configService: ConfigService) { }

  public getService<T>(url: string): Observable<T> {
    return new Observable<T>((observer: Observer<T>) => {
      this.http
        .get<T>(
          `${this.configService.baseUrl}${url}`,
          httpOptions
        )
        .subscribe(
          (result) => {
              observer.next(result);
              observer.complete();
          }
        ),err=>{
          observer.error(err);
        };
    });
  }
}
