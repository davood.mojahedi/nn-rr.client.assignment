import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Config } from '../data/config';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ConfigService extends Config {
  constructor(private http: HttpClient) {
    super();
  }

  public load() {
    let _env: string = "development";
    if (environment.production)
      _env = "production"
    this.http.get<Config>(`../../../assets/config/${_env}.json`)
        .subscribe(result => {
          this.baseUrl = result.baseUrl
        },

        error => {
          console.log(error);
        })
    }
}
