import { Quote } from './../data/quote';
import { Company } from './../data/company';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseHttpApiService } from './base-http-api.service';

@Injectable({
  providedIn: 'root'
})
export class QuoteInfoService {

  constructor(private baseHttpApiService:BaseHttpApiService) { }

  public getAllCompanies(): Observable<Company[]> {
    return this.baseHttpApiService.getService<Company[]>('/quote/company/list');
  }
  
  public getQuoteInfo(symbol:string): Observable<Quote> {
    return this.baseHttpApiService.getService<Quote>('/quote/info/'+symbol);
  }

}
